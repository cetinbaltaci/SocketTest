ARCH=x86
ifeq ($(ARCH),arm)
CROSS_COMPILE=arm-none-linux-gnueabi-
EXTRA_CXXFLAGS= 
EXTRA_LIBS= 
EXTRA_DEF=
else
CROSS_COMPILE=
EXTRA_CXXFLAGS=
EXTRA_LIBS= 
EXTRA_DEF=
endif
CXX = $(CROSS_COMPILE)g++
CXXFLAGS =	-O2 -g -Wall -fmessage-length=0 $(EXTRA_CXXFLAGS) $(EXTRA_DEF)
OBJS =	SocketTest.o SocketUtil.o
LIBS = $(EXTRA_LIBS)

TARGET =	SocketTest

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
