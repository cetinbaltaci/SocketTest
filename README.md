SocketTest
Client socket programs: Blocking, nonblocking, and asynchronous socket calls.
Project Features:
	- Socket API (connect, ...)
	- Block / Nonblock mode
		- EPOLL
		- POLL
	- x86 / arm compile support
