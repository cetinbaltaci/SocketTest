//============================================================================
// Name        : SocketTest.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "SocketUtil.h"

#define MAX_TEST_COUNT	1

int main(int argc, char *argv[]) {
	printf("Socket Client Test App.\n");
	if (argc != 3 ) {
		printf("Usage: %s TARGET_HOST TARGET_PORT\n", argv[0]);
		return EXIT_FAILURE;
	}

	char *host = argv[1] ;
	int port = atoi(argv[2]);

	for(int inx = 0 ; inx < MAX_TEST_COUNT; inx++) {
		printf("(%d)Start\n" , inx);
		int sock = SocketOpen();
		if (sock == -1) {
			printf("[%s] SocketOpen error.\n" , EVENT_TYPE_NAME);
			return EXIT_FAILURE ;
		}
		printf("[%s] SocketOpen OK.\n" , EVENT_TYPE_NAME);

		time_t t1 = time(NULL);
		int r = SocketConnect(sock, host, port, 10);
		printf("[%s] SocketConnect -> Ret: %d Time: %d\n",EVENT_TYPE_NAME, r, (int)time(NULL) - (int)t1 ) ;

		const char *TEST_DATA = "#OliveBranchOperation\n" ;
		if ( r == 0 ) {
			t1 = time(NULL);
			r = SocketSend(sock, (unsigned char *)TEST_DATA, strlen(TEST_DATA), 5 ) ;
			printf("[%s] SocketSend -> Ret: %d Time: %d\n",EVENT_TYPE_NAME, r, (int)time(NULL) - (int)t1 ) ;
			int bufLen = 0 ;
			unsigned char *buf = NULL ;
			t1 = time(NULL);
			r = SocketReceive(sock, &buf, &bufLen, 10) ;
			printf("[%s] SocketReceive -> Ret: %d Time: %d\n",EVENT_TYPE_NAME, r, (int)time(NULL) - (int)t1 ) ;
			if (r == 0 && buf) {
				printf("Recv: %s\n", (char *)buf) ;
			}
			if (buf) free(buf);
		}
		SocketClose(sock) ;
		printf("(%d)End\n\n\n" , inx);
		sleep(3);
	}
	return EXIT_SUCCESS;
}
