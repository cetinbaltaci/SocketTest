/*
 * SocketUtil.cpp
 *
 *  Created on: Feb 27, 2018
 *      Author: adminuser
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <poll.h>
#include <sys/epoll.h>
#include <errno.h>
#include <netdb.h>
#include <sys/ioctl.h>

int enableSocketNonBlocking(int sock, int enable) ;

int openSocket(int nonblock) {
	int hSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (hSocket == -1 ) return hSocket ;
	int enable = 1 ;
	setsockopt(hSocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable) );
	setsockopt(hSocket, SOL_SOCKET, SO_KEEPALIVE, &enable, sizeof(enable) );

	if (nonblock) enableSocketNonBlocking(hSocket, 1);

	return hSocket ;
}

int connectSocket(int sock, char *targetIP, int targetPort, int timeout) {
	(void )timeout;
	struct sockaddr_in srv_adr;
	bzero((void *) &srv_adr, sizeof(srv_adr));
	srv_adr.sin_family = AF_INET;
	srv_adr.sin_port = htons(targetPort);
	struct hostent  *he = NULL ;
	if ( (he = gethostbyname(targetIP)) == NULL ) {
		srv_adr.sin_addr.s_addr = inet_addr(targetIP);
	}else {
		memcpy(&srv_adr.sin_addr, he->h_addr_list[0], he->h_length);
	}
	printf("[%s:%d] IP Address: %s\n",__FUNCTION__, __LINE__,  inet_ntoa( srv_adr.sin_addr ) );
    struct timeval _timeval;
    _timeval.tv_sec = timeout;
    _timeval.tv_usec = 0;
	setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO,  (void *)&_timeval, sizeof(_timeval));

	return connect(sock , (struct sockaddr *)&srv_adr , sizeof(srv_adr)) ;
}


int connectSocket_epoll(int sock, char *targetIP, int targetPort, int timeout) {
	struct sockaddr_in srv_adr;
	bzero((void *) &srv_adr, sizeof(srv_adr));
	srv_adr.sin_family = AF_INET;
	srv_adr.sin_port = htons(targetPort);
	struct hostent  *he = NULL ;
	if ( (he = gethostbyname(targetIP)) == NULL ) {
		srv_adr.sin_addr.s_addr = inet_addr(targetIP);
	}else {
		memcpy(&srv_adr.sin_addr, he->h_addr_list[0], he->h_length);
	}
	printf("[%s:%d] IP Address: %s\n",__FUNCTION__, __LINE__, inet_ntoa( srv_adr.sin_addr ) );

	const int POLL_SIZE = 1 ;
	struct epoll_event ev, events[POLL_SIZE];

	int epollfd = epoll_create(1);

	ev.events = EPOLLOUT;
	ev.data.fd = sock;
	epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev);
	int ret = connect(sock , (struct sockaddr *)&srv_adr , sizeof(srv_adr)) ;
	if (ret == -1) {
		if (errno == EINPROGRESS) {
			while(1) {
				ret = epoll_wait(epollfd, events, POLL_SIZE, timeout * 1000) ;
				if (ret == -1 ) {
					if (errno == EINTR ) continue ;
				}else if (ret == 0 ) {
					printf("[%s:%d] Connect Timeout\n",__FUNCTION__, __LINE__);
					ret = -1 ;
				}else {
					if (events[0].events == 0 || events[0].data.fd != sock) continue ;
					if (events[0].events & (EPOLLERR | EPOLLHUP)) {
						ret = -1 ;
					}else if (events[0].events & EPOLLOUT) {
						int error_code ;
						unsigned int error_code_size = sizeof(error_code) ;
						if ( getsockopt(sock, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size) == 0 ) {
							ret = error_code;
						}
					}
				}
				break ;
			}
		}
	}
	close(epollfd);
	return ret ;
}

int connectSocket_poll(int sock, char *targetIP, int targetPort, int timeout) {
	struct sockaddr_in srv_adr;
	bzero((void *) &srv_adr, sizeof(srv_adr));
	srv_adr.sin_family = AF_INET;
	srv_adr.sin_port = htons(targetPort);
	struct hostent  *he = NULL ;
	if ( (he = gethostbyname(targetIP)) == NULL ) {
		srv_adr.sin_addr.s_addr = inet_addr(targetIP);
	}else {
		memcpy(&srv_adr.sin_addr, he->h_addr_list[0], he->h_length);
	}
	printf("[%s:%d] IP Address: %s\n",__FUNCTION__, __LINE__, inet_ntoa( srv_adr.sin_addr ) );

	const int POLL_SIZE = 1 ;
	struct pollfd poll_set[POLL_SIZE];
	poll_set[0].fd = sock ;
	poll_set[0].events = POLLOUT ;
	poll_set[0].revents = 0;

	int ret = connect(sock , (struct sockaddr *)&srv_adr , sizeof(srv_adr)) ;
	if (ret == -1) {
		if (errno == EINPROGRESS) {
			while(1) {
				ret = poll(poll_set, POLL_SIZE, timeout * 1000) ;
				if (ret == -1 ) {
					if (errno == EINTR ) continue ;
				}else if (ret == 0 ) {
					printf("[%s:%d] Connect Timeout\n",__FUNCTION__, __LINE__);
					ret = -1 ;
				}else {
					if (poll_set[0].revents == 0 || poll_set[0].fd != sock) continue ;

					if (poll_set[0].revents & (POLLERR | POLLHUP | POLLNVAL )) {
						ret = -1 ;
					}else if (poll_set[0].revents & POLLOUT) {
						int error_code ;
						unsigned int error_code_size = sizeof(error_code) ;
						if ( getsockopt(sock, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size) == 0 ) {
							ret = error_code;
						}
					}
				}
				break ;
			}
		}
	}
	return ret ;
}

int enableSocketNonBlocking(int sock, int enable) {
	int flags = fcntl(sock, F_GETFL);
	if (enable) {
		flags  |= O_NONBLOCK ;
	}else {
		flags  &= ~O_NONBLOCK ;
	}
	return fcntl(sock, F_SETFL, flags);
}


void closeSocket(int sock) {
	close(sock);
}

int sendSocket(int sock, unsigned char *buff, int buffLen, int timeout) {
    struct timeval _timeval;
    _timeval.tv_sec = timeout;
    _timeval.tv_usec = 0;
	setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO,  (void *)&_timeval, sizeof(_timeval));
	return send(sock, buff, buffLen, 0);
}


int sendSocket_epoll(int sock, unsigned char *buff, int buffLen, int timeout) {
	const int POLL_SIZE = 1 ;
	struct epoll_event ev, events[POLL_SIZE];
	int epollfd = epoll_create(1);

	ev.events = EPOLLOUT;
	ev.data.fd = sock;
	epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev);
	int ret = send(sock, buff, buffLen, MSG_DONTWAIT);
	printf("[%s:%d] send -> ret : %d\n"  ,__FUNCTION__, __LINE__,ret) ;
	while(1) {
		ret = epoll_wait(epollfd, events, POLL_SIZE, timeout * 1000) ;
		if (ret == -1 && errno == EINTR) continue;
		if (ret == 0 ){
			printf("[%s:%d] send timeout\n" ,__FUNCTION__, __LINE__) ;
			ret = -1 ;
		}else {
			printf("[%s:%d] events[0].data.fd: %d events[0].events: %x\n" ,__FUNCTION__, __LINE__,events[0].data.fd, events[0].events ) ;
			if (events[0].events == 0 || events[0].data.fd != sock) continue ;
			if (events[0].events & (EPOLLERR | EPOLLHUP )) {
				printf("[%s:%d] (EPOLLERR | EPOLLHUP )\n" ,__FUNCTION__, __LINE__) ;
				ret = -2 ;
			}else if (events[0].events & EPOLLOUT) {
				ret = 0 ;
				printf("[%s:%d] send Success\n" ,__FUNCTION__, __LINE__) ;
			}else{
				printf("[%s:%d] send unknown error\n" ,__FUNCTION__, __LINE__) ;
				ret = -3 ;
			}
		}
		break ;
	}
	close(epollfd);

	return ret ;
}

int sendSocket_poll(int sock, unsigned char *buff, int buffLen, int timeout) {
	const int POLL_SIZE = 1 ;
	struct pollfd poll_set[POLL_SIZE];
	poll_set[0].fd = sock ;
	poll_set[0].events = POLLOUT ;
	poll_set[0].revents = 0;

	int ret = send(sock, buff, buffLen, MSG_DONTWAIT);
	printf("[%s:%d] send -> ret : %d\n" ,__FUNCTION__, __LINE__, ret) ;
	while(1) {

		ret = poll(poll_set, POLL_SIZE, timeout * 1000) ;
		if (ret == -1 && errno == EINTR) continue;
		if (ret == 0 ){
			printf("[%s:%d] send timeout\n", __FUNCTION__, __LINE__) ;
			ret = -1 ;
		}else {
			printf("[%s:%d] poll_set[0].fd: %d poll_set[0].revents: %x\n" ,__FUNCTION__, __LINE__,poll_set[0].fd, poll_set[0].revents ) ;
			if (poll_set[0].revents == 0 || poll_set[0].fd != sock) continue ;
			if (poll_set[0].revents & (POLLERR | POLLHUP | POLLNVAL )) {
				printf("[%s:%d] (POLLERR | POLLHUP | POLLNVAL )\n", __FUNCTION__, __LINE__) ;
				ret = -2 ;
			}else if (poll_set[0].revents & POLLOUT) {
				ret = 0 ;
				printf("[%s:%d] send Success\n", __FUNCTION__, __LINE__) ;
			}else{
				printf("[%s:%d] send unknown error\n", __FUNCTION__, __LINE__) ;
				ret = -3 ;
			}
		}
		break ;
	}
	return ret ;
}

int recvSocket(int sock, unsigned char **buff, int *buffLen, int timeout) {
	(void)timeout;
	int max_buf_len = 512 ;
    struct timeval _timeval;
    _timeval.tv_sec = timeout;
    _timeval.tv_usec = 0;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,  (void *)&_timeval, sizeof(_timeval));
	*buff = (unsigned char *)calloc(max_buf_len, sizeof(unsigned char));
	*buffLen = recv(sock, *buff , max_buf_len , 0) ;
	return (*buffLen > 0 )? 0 : -1 ;
}

int recvSocket_epoll(int sock, unsigned char **buff, int *buffLen, int timeout) {
	const int POLL_SIZE = 1 ;
	struct epoll_event ev, events[POLL_SIZE];
	int epollfd = epoll_create(1);

	ev.events = EPOLLIN;
	ev.data.fd = sock;
	epoll_ctl(epollfd, EPOLL_CTL_ADD, sock, &ev);

	int ret = -1 ;
	while(1) {
		ret = epoll_wait(epollfd, events, POLL_SIZE, timeout * 1000) ;
		if (ret == -1 && errno == EINTR) continue;
		if (ret == 0 ){
			printf("[%s:%d] recv timeout\n",__FUNCTION__, __LINE__) ;
			ret = -2 ;
		}else {
			printf("[%s:%d] events[0].data.fd: %d events[0].events: %x\n" ,__FUNCTION__, __LINE__, events[0].data.fd, events[0].events ) ;
			if (events[0].events == 0 || events[0].data.fd != sock) continue ;
			if (events[0].events & (EPOLLERR | EPOLLHUP )) {
				printf("[%s:%d] (EPOLLERR | EPOLLHUP )\n", __FUNCTION__, __LINE__) ;
				ret = -3 ;
			}else if (events[0].events & EPOLLIN) {
				int nread = 0 ;
				*buffLen = 0 ;
				ret = ioctl(sock, FIONREAD, &nread);
				printf("[%s:%d] ioctl -> ret:%d nread:%d\n", __FUNCTION__, __LINE__, ret , nread) ;
				if ( ret == 0 && nread > 0 ) {
					*buff = (unsigned char *)calloc(nread, sizeof(unsigned char ));
					*buffLen = read(sock, *buff, nread) ;
					if ( *buffLen > 0 )
						ret = 0 ;
					else
						ret = -4 ;

				}else {
					ret = -5 ;
				}

			}else{
				printf("[%s:%d] recv unknown error\n",  __FUNCTION__, __LINE__) ;
				ret = -6 ;
			}
		}
		break ;
	}
	close(epollfd);
	return ret ;
}

int recvSocket_poll(int sock, unsigned char **buff, int *buffLen, int timeout) {
	const int POLL_SIZE = 1 ;
	struct pollfd poll_set[POLL_SIZE];
	poll_set[0].fd = sock ;
	poll_set[0].events = POLLIN;
	poll_set[0].revents = 0;
	int ret = -1 ;
	while(1) {
		ret = poll(poll_set, POLL_SIZE, timeout * 1000) ;
		if (ret == -1 && errno == EINTR) continue;
		if (ret == 0 ){
			printf("[%s:%d] recv timeout\n", __FUNCTION__, __LINE__) ;
			ret = -2 ;
		}else {
			printf("[%s:%d] poll_set[0].fd: %d poll_set[0].revents: %x\n" , __FUNCTION__, __LINE__, poll_set[0].fd, poll_set[0].revents ) ;
			if (poll_set[0].revents == 0 || poll_set[0].fd != sock) continue ;
			if (poll_set[0].revents & (POLLERR | POLLHUP | POLLNVAL )) {
				printf("[%s:%d] (POLLERR | POLLHUP | POLLNVAL )\n", __FUNCTION__, __LINE__) ;
				ret = -3 ;
			}else if (poll_set[0].revents & POLLIN) {
				int nread = 0 ;
				*buffLen = 0 ;
				ret = ioctl(sock, FIONREAD, &nread);
				printf("[%s:%d] ioctl -> ret:%d nread:%d\n",  __FUNCTION__, __LINE__, ret , nread) ;
				if ( ret == 0 && nread > 0 ) {
					*buff = (unsigned char *)calloc(nread, sizeof(unsigned char ));
					*buffLen = read(sock, *buff, nread) ;
					if ( *buffLen > 0 )
						ret = 0 ;
					else
						ret = -4 ;

				}else {
					ret = -5 ;
				}

			}else{
				printf("[%s:%d] recv unknown error\n",  __FUNCTION__, __LINE__) ;
				ret = -6 ;
			}
		}
		break ;
	}
	return ret ;
}

