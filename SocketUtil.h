/*
 * SocketUtil.h
 *
 *  Created on: Feb 27, 2018
 *      Author: adminuser
 */

#ifndef SOCKETUTIL_H_
#define SOCKETUTIL_H_


int openSocket(int nonblock);
int connectSocket(int sock, char *targetIP, int targetPort, int timeout);
int connectSocket_epoll(int sock, char *targetIP, int targetPort, int timeout);
int connectSocket_poll(int sock, char *targetIP, int targetPort, int timeout);

int sendSocket(int sock, unsigned char *buff, int buffLen, int timeout);
int sendSocket_epoll(int sock, unsigned char *buff, int buffLen, int timeout);
int sendSocket_poll(int sock, unsigned char *buff, int buffLen, int timeout);

int recvSocket(int sock, unsigned char **buff, int *buffLen, int timeout);
int recvSocket_epoll(int sock, unsigned char **buff, int *buffLen, int timeout);
int recvSocket_poll(int sock, unsigned char **buff, int *buffLen, int timeout);

void closeSocket(int sock);

#define NO_EVENT		0
#define POLL_EVENT		1
#define EPOLL_EVENT		2

#define EVENT_TYPE		EPOLL_EVENT

#if (EVENT_TYPE == POLL_EVENT)
#define EVENT_TYPE_NAME		"POLL_EVENT"
#define SocketOpen()	openSocket(1)
#define SocketConnect(sock,	targetIP, targetPort, timeout ) connectSocket_poll(sock,	targetIP, targetPort, timeout)
#define SocketSend(sock, buff, buffLen, timeout)  sendSocket_poll(sock, buff, buffLen, timeout)
#define SocketReceive(sock, buff, buffLen, timeout) recvSocket_poll(sock, buff, buffLen, timeout)
#define SocketClose(sock) closeSocket(sock)
#elif (EVENT_TYPE == EPOLL_EVENT)
#define EVENT_TYPE_NAME		"EPOLL_EVENT"
#define SocketOpen()	openSocket(1)
#define SocketConnect(sock,	targetIP, targetPort, timeout ) connectSocket_epoll(sock,	targetIP, targetPort, timeout)
#define SocketSend(sock, buff, buffLen, timeout)  sendSocket_epoll(sock, buff, buffLen, timeout)
#define SocketReceive(sock, buff, buffLen, timeout) recvSocket_epoll(sock, buff, buffLen, timeout)
#define SocketClose(sock) closeSocket(sock)
#else
#define EVENT_TYPE_NAME		"NO_EVENT"
#define SocketOpen() openSocket(0)
#define SocketConnect(sock,	targetIP, targetPort, timeout ) connectSocket(sock,	targetIP, targetPort, timeout)
#define SocketSend(sock, buff, buffLen, timeout)  sendSocket(sock, buff, buffLen, timeout)
#define SocketReceive(sock, buff, buffLen, timeout) recvSocket(sock, buff, buffLen, timeout)
#define SocketClose(sock) closeSocket(sock)
#endif


#endif /* SOCKETUTIL_H_ */
